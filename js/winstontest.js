var winston = require("winston");
const logger = winston.createLogger({
  transports: [new winston.transports.Console({'level': 'error',
    'format': winston.format.combine(
      winston.format.timestamp({format: 'YYYY-MM-DD HH:mm'}),
        winston.format.label({label: 'logger'}),
        winston.format.printf((msg) => {
          return `${msg.timestamp} - ${msg.label} [${msg.level}]:${JSON.stringify(msg.message)}`;
        }),
        winston.format.colorize({all: true}),
    ),
  })],
});
const logger2 = winston.createLogger({
  transports: [new winston.transports.Console({'level': 'error',
    'format': winston.format.combine(
        winston.format.timestamp({format: 'YYYY-MM-DD HH:mm'}),
        winston.format.json(),
        winston.format.label({label: 'logger2'}),
        winston.format.printf((msg) => {
          return `${msg.timestamp} - ${msg.label} [${msg.level}]:${JSON.stringify(msg.message)}`;
        }),
        winston.format.colorize({all: true}),
    ),
  })],
});
const logger3 = winston.createLogger({
  transports: [new winston.transports.Console({'level': 'error',
    'format': winston.format.combine(
        winston.format.timestamp({format: 'YYYY-MM-DD HH:mm'}),
        winston.format.splat(),
        winston.format.label({label: 'logger3'}),
        winston.format.simple(), //simple is the only way I have found that allows CRLF injection
        winston.format.colorize({all: true}),
    ),
  })],
});
const logger4 = winston.createLogger({
  transports: [new winston.transports.Console({'level': 'error',
    'format': winston.format.combine(
        winston.format.timestamp({format: 'YYYY-MM-DD HH:mm'}),
        winston.format.label({label: 'logger4'}),
        winston.format.colorize({all: true}),
    ),
  })],
});
var CRLFMessage = "TestingCRLF\r\nline2\%0a%0dline3\\u2028\\u2029line4\\r\\nline5";
logger.error(CRLFMessage);
logger2.error(CRLFMessage);
logger3.error(CRLFMessage);
logger4.error("logger4" + CRLFMessage);

/*
2020-10-21 16:22 - logger [error]:"TestingCRLF\r\nline2%0a%0dline3\\u2028\\u2029line4\\r\\nline5"
2020-10-21 16:22 - logger2 [error]:"TestingCRLF\r\nline2%0a%0dline3\\u2028\\u2029line4\\r\\nline5"
error: TestingCRLF
line2%0a%0dline3\u2028\u2029line4\r\nline5 {"timestamp":"2020-10-21 16:22","label":"logger3"}
{"message":"logger4TestingCRLF\r\nline2%0a%0dline3\\u2028\\u2029line4\\r\\nline5","level":"error"}
*/