const express = require('express');
var csurf = require('csurf');

var winston = require("winston");
// FIX CWE-117: Improper Output Neutralization for Logs
const replacer =function(key, value) {
  return value.replace(/(?:\\[rn]|[\r\n]+)+/g, '_');
};
const logger = winston.createLogger({
  transports: [new winston.transports.Console({'level': 'error',
    'format': winston.format.combine(
      winston.format.timestamp({format: 'YYYY-MM-DD HH:mm'}),
        winston.format.label({label: 'logger'}),
        winston.format.printf((msg) => {
          //return `${msg.timestamp} [${msg.level}]:${JSON.stringify(msg.message, replacer)}`;
          return `${msg.timestamp} - ${msg.label} [${msg.level}]:${JSON.stringify(msg.message)}`;
        }),
        winston.format.colorize({all: true}),
    ),
  })],
});
const logger2 = winston.createLogger({
  transports: [new winston.transports.Console({'level': 'error',
    'format': winston.format.combine(
        winston.format.timestamp({format: 'YYYY-MM-DD HH:mm'}),
        winston.format.json(),
        winston.format.label({label: 'logger2'}),
        winston.format.printf((msg) => {
          return `${msg.timestamp} - ${msg.label} [${msg.level}]:${JSON.stringify(msg.message)}`;
        }),
        winston.format.colorize({all: true}),
    ),
  })],
});
const logger3 = winston.createLogger({
  transports: [new winston.transports.Console({'level': 'error',
    'format': winston.format.combine(
        winston.format.timestamp({format: 'YYYY-MM-DD HH:mm'}),
        winston.format.splat(),
        winston.format.label({label: 'logger3'}),
        winston.format.simple(), //simple is the only way I have found that allows CRLF injection
        winston.format.colorize({all: true}),
    ),
  })],
});
const logger4 = winston.createLogger({
  transports: [new winston.transports.Console({'level': 'error',
    'format': winston.format.combine(
        winston.format.timestamp({format: 'YYYY-MM-DD HH:mm'}),
        winston.format.label({label: 'logger4'}),
        winston.format.colorize({all: true}),
    ),
  })],
});

const app = express();
const https = require('https')
const fs = require('fs');

https.createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
}, app).listen(3000, () => {
  console.log('Listening...')
})


const port = 3000;
const path = require('path');
const trustedRoot = "/Users/ayoung/Apps/js/cwe-73/";
const createDOMPurify = require('dompurify');
const { JSDOM } = require('jsdom');
const window = new JSDOM('').window;
const DOMPurify = createDOMPurify(window);
var myargs = process.argv.slice(2);
console.log(myargs)
var fullPath = trustedRoot + myargs[0];
console.log(__dirname.toString());
console.log ("normalized path: " + path.normalize(fullPath));
console.log ("Inside trusted root? " + path.normalize(fullPath).startsWith(trustedRoot))
function jsEscape(str) {
  return String(str).replace(/[^\w. ]/gi, function (c) {
      console.log(c.toString() + ":" + '\\u' + ('0000' + c.charCodeAt(0).toString(16)).slice(-4));
      return '\\u' + ('0000' + c.charCodeAt(0).toString(16)).slice(-4);
  });
}
//if(path.normalize(fullPath).startsWith(trustedRoot))
//fs.readFile('/Users/joe/test.txt', (err, data) => {
fs.readFile(trustedRoot + myargs[0], (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  console.log(data)
})

app.listen(8000, function (err) {
  if (err) { 
    console.log(err);
 } else {
    console.log("App started at port 8000");
 }    
})

app.get('/', (req, res) => {
  console.log(req.params);
  console.log(req.params.id);
  res.send(req.params.id);
})
app.get('/test/:id', (req, res) => {
  console.log(req.params);
  console.log(req.params.id);
  res.writeHead(200, {
    //'Content-Type': 'text/html' //pops an alert
    'Content-Type': 'text/plain', //does not
    'X-Content-Type-Options': 'nosniff'
  })
  res.end(req.params.id);
})
//http://localhost:8000/user/yep/xss
app.get('/user/:id/:test', function (req, res) {
  //logger.error(req.params.id);
  var CRLFMessage = "TestingCRLF\r\nline2\%0a%0dline3\\u2028\\u2029line4\\r\\nline5";
  logger.error(CRLFMessage);
  logger2.error(CRLFMessage);
  logger3.error(CRLFMessage);
  logger4.error("logger4" + CRLFMessage);
  //console.log(req.params.id);
  //console.log(encodeURI(req.params.id));
  //console.log(encodeURIComponent(req.params.id));
  var id = jsEscape(req.params.id);
  var test = jsEscape(req.params.test);
  res.setHeader("test", req.params.id);
  res.setHeader("noCRLF", encodeURI(req.params.id));

  var htmlDoc=`<!DOCTYPE html>
  <html>
  <body>
  
  <h1 class="${id}">${id}</h1>
  <p>${test}</p>
  
  </body>
  </html>
  
  `
  //<h2>${req.params.id}</h>
  //<p>${req.params.test}</p>
  //res.send(DOMPurify.sanitize(htmlDoc, { USE_PROFILES: {html:true}}));
  res.send(htmlDoc);
})